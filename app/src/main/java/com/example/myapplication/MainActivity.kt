class Car {
    var model: String = ""
    var color: String = ""
    var engine: Float = 0F

    fun printcolor() {
        println(color)
    }


    override fun equals(other: Any?): Boolean {
        if (other is Car) {
            if (engine == other.engine) {
                return true
            }
        }
        return false
    }
}


fun main() {
    var car1 = Car()
    car1.model = "BMW"
    car1.color = "Black"
    car1.engine = 5.0F